#ifndef __VERSION_H
#define __VERSION_H

/* Getters for version numbers. */

int get_version_major(void);
int get_version_minor(void);
int get_version_patch(void);
const char* get_version_string(void);

#endif //__VERSION_H